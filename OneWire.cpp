/*
 * 1Wire.cpp
 *
 *  Created on: Aug. 12, 2020
 *      Author: amr
 */

#include <msp430.h>
#include <intrinsics.h>
#include <OneWire.hpp>

OneWire::OneWire() {
    // set up GPIO
    P2REN |= BIT2; // pullup when set as input
    P2DIR |= BIT2; // OWD on P2.0
    P2OUT |= BIT2; // Pull high to charge
    return;
}

uint8_t OneWire::resetBus() {
    P2OUT &= ~BIT2;
    __delay_cycles(RESET_LENGTH);
    P2OUT |= BIT2;
    P2DIR &= ~BIT2; // set as input
    __delay_cycles(PRESENCE_LENGTH);
    if(!(P2IN & BIT2)) {
        P2DIR |= BIT2;
        __delay_cycles(RESET_LENGTH);
        return 0;
    } else {
        P2DIR |= BIT2;
        __delay_cycles(RESET_LENGTH);
        return 1;
    }
}

void OneWire::transact(uint8_t *buffer, uint16_t length, transactionType type) {
    uint8_t byteBuffer = 0;
    switch(type) {
    case READ:
        for(uint16_t i = 0; i < length; i++) {
            for(uint8_t j = 0; j < 8; j++) {
                P2OUT &= ~BIT2;
                __delay_cycles(READ_LENGTH);
                P2OUT |= BIT2;
                P2DIR &= ~BIT2; // set as input with pullup
                __delay_cycles(READ_WAIT_LENGTH); // wait long enough for a response
                if(P2IN & BIT2) {
                    byteBuffer |= (1 << j);
                }
                P2DIR |= BIT2;
                __delay_cycles(READ_POST_LENGTH); // give a little time before starting the next bit read
            }
            *(buffer + i) = byteBuffer;
            byteBuffer = 0;
        }
        break;
    case WRITE:
        for(uint16_t i = 0; i < length; i++) {
            byteBuffer = *(buffer + i);
            for(uint8_t j = 0; j < 8; j++) {
                if((byteBuffer >> j) & 0x01) { // Send a 1
                    P2OUT &= ~BIT2;
                    __delay_cycles(ONE_LOW_LENGTH);
                    P2OUT |= BIT2;
                    __delay_cycles(ONE_HIGH_LENGTH);
                } else { // send a 0
                    P2OUT &= ~BIT2;
                    __delay_cycles(ZERO_LOW_LENGTH);
                    P2OUT |= BIT2;
                    __delay_cycles(ZERO_HIGH_LENGTH);
                }
                __delay_cycles(BIT_PERIOD_LENGTH);
            }
        }
        break;
    case WRITEREAD:
        break;
    case TEST:
        P2OUT &= ~BIT2;
        __delay_cycles(ONE_LOW_LENGTH);
        P2OUT |= BIT2;
        __delay_cycles(ONE_HIGH_LENGTH);
        P2OUT &= ~BIT2;
        __delay_cycles(ZERO_LOW_LENGTH);
        P2OUT |= BIT2;
        __delay_cycles(ZERO_HIGH_LENGTH);

        break;
    default:
        __never_executed();
    }
    return;
}

uint8_t OneWire::compareROM(uint8_t *storedROM, uint8_t *receivedROM) {
    uint8_t match = 1;
    for(uint8_t i = 0; i < 8; i++) {
        if(*(receivedROM + i) != *(storedROM + i)) {
            match = 0;
            return match;
        }
    }
    return match;
}
