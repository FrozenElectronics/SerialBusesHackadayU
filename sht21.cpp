/*
 * sht21.cpp
 *
 *  Created on: Jul. 22, 2020
 *      Author: amr
 */

#include <sht21.hpp>
#include <msp430.h>
#include <intrinsics.h>
#include <cstdlib>

sht21::sht21(i2c *givenBus) {
    this->busAccess = givenBus;
}

double sht21::getTemp(void) {
    double temperature;
    uint8_t status = 0;
    uint16_t rawTemp = 0;
    uint8_t *buffer = (uint8_t *)calloc(10, sizeof(uint8_t));
    busAccess->changeAddress(sht21Address);
    buffer[0] = TRIGGER_TEMP;
    busAccess->transact(buffer, 1, WRITE);

    do {
        status = busAccess->transact(buffer, 3, READ);
        __delay_cycles(15000);
    } while(status == NACK_ADDR) ; // poll for conversion
    rawTemp = (buffer[0] << 8) | (buffer[1] & 0xFC);
    temperature = -46.85 + (175.72 * ((double)rawTemp / 65535.0f));

    free(buffer);
    return temperature;
}
double sht21::getHumid(void) {
    double humidity = 0;
    uint8_t status = 0;
    uint16_t rawHumid = 0;
    uint8_t *buffer = (uint8_t *)calloc(10, sizeof(uint8_t));
    busAccess->changeAddress(sht21Address);
    buffer[0] = TRIGGER_HUMID;
    busAccess->transact(buffer, 1, WRITE);

    do {
        status = busAccess->transact(buffer, 3, READ);
        __delay_cycles(15000);
    } while(status == NACK_ADDR) ; // poll for conversion
    rawHumid = (buffer[0] << 8) | (buffer[1] & 0xFC);
    humidity = -6.0 + (125.0 * ((double)rawHumid / 65535.0f));

    free(buffer);
    return humidity;
}

void sht21::heaterOn(void) {
    uint8_t *buffer = (uint8_t *)calloc(10, sizeof(uint8_t));
    busAccess->changeAddress(sht21Address);
    buffer[0] = WRITE_REG;
    buffer[1] = 0x03;
    busAccess->transact(buffer, 2, WRITE);
    free(buffer);
    return;
}

void sht21::heaterOff(void) {
    uint8_t *buffer = (uint8_t *)calloc(10, sizeof(uint8_t));
    busAccess->changeAddress(sht21Address);
    buffer[0] = WRITE_REG;
    buffer[1] = 0x01;
    busAccess->transact(buffer, 2, WRITE);
    free(buffer);
    return;
}
