/*
 * bmp180.cpp
 *
 *  Created on: Aug. 15, 2020
 *      Author: amr
 */

#include <cstdlib>
#include <bmp180.hpp>
#include <i2c.hpp>

bmp180::bmp180(i2c *givenBus) {
    uint8_t *buffer = (uint8_t *)malloc(sizeof(uint8_t) * 22);
    uint8_t reg = 0xAA;
    this->busAccess = givenBus;

    busAccess->changeAddress(bmp180Addr);
    busAccess->transact(&reg, 1, WRITE);
    busAccess->transact(buffer, 22, READ);
    // These are the calibration coefficients for this particular chip
    // They only need to be read once, so we do it here
    AC1 = *(buffer) << 8 | *(buffer + 1);
    AC2 = *(buffer + 2) << 8 | *(buffer + 3);
    AC3 = *(buffer + 4) << 8 | *(buffer + 5);
    AC4 = *(buffer + 6) << 8 | *(buffer + 7);
    AC5 = *(buffer + 8) << 8 | *(buffer + 9);
    AC6 = *(buffer + 10) << 8 | *(buffer + 11);
    B1 = *(buffer + 12) << 8 | *(buffer + 13);
    B2 = *(buffer + 14) << 8 | *(buffer + 15);
    MB = *(buffer + 16) << 8 | *(buffer + 17);
    MC = *(buffer + 18) << 8 | *(buffer + 19);
    MD = *(buffer + 20) << 8 | *(buffer + 21);

    free(buffer);
}

void bmp180::getPressure(double *pressure, double *temperature) {
    uint8_t *buffer = (uint8_t *)malloc(sizeof(uint8_t) * 3);
    uint32_t UT, UP;
    uint8_t command[] = { 0xF4, SAMPLE_TEMPERATURE };
    long X1, X2, X3, B5, T, B6, B3, p;
    unsigned long B4, B7;
    uint8_t oss = 0x03;

    busAccess->changeAddress(bmp180Addr);
    busAccess->transact(command, 2, WRITE);
    // wait 4.5ms
    __delay_cycles(1e5); // should be about 5ms?
    command[0] = 0xF6;
    busAccess->transact(command, 1, WRITE);
    busAccess->transact(buffer, 2, READ);
    UT = (*buffer << 8) + *(buffer + 1);

    command[0] = 0xF4;
    command[1] = SAMPLE_PRESSURE;
    busAccess->transact(command, 2, WRITE);
    // wait 25.5ms
    __delay_cycles(6e5);
    command[0] = 0xF6;
    busAccess->transact(command, 1, WRITE);
    busAccess->transact(buffer, 3, READ);

    UP = *(buffer);
    UP <<= 8;
    UP |= *(buffer + 1);
    UP <<= 8;
    UP |= *(buffer + 2);
    UP >>= 8 - oss;

    const int32_t TWO_FOUR = 16;
    const int32_t TWO_EIGHT = 256;
    const int32_t TWO_ELEVEN = 2048;
    const int32_t TWO_TWELVE = 4096;
    const int32_t TWO_THIRTEEN = 8192;
    const int32_t TWO_FIFTEEN = 32768;
    const int32_t TWO_SIXTEEN = 65536;
    // CALCULATE TEMPERATURE
    X1 = (UT - AC6) * AC5 / TWO_FIFTEEN;
    X2 = (MC * TWO_ELEVEN) / (X1 + MD);
    B5 = X1 + X2;
    T = (B5 + 8) / TWO_FOUR;
    // CALCULATE PRESSURE
    B6 = B5 - 4000;
    X1 = (B2 * (B6 * B6 / TWO_TWELVE)) / TWO_ELEVEN;
    X2 = AC2 * B6 / TWO_ELEVEN;
    X3 = X1 + X2;
    B3 = (((AC1 * 4 + X3) << oss) + 2) / 4;
    X1 = AC3 * B6 / TWO_THIRTEEN;
    X2 = (B1 * (B6 * B6 / TWO_TWELVE)) / TWO_SIXTEEN;
    X3 = ((X1 + X2) + 2) / 4;
    B4 = AC4 * (unsigned long)(X3 + 32768) / TWO_FIFTEEN;
    B7 = ((unsigned long)UP - B3) * (50000 >> oss);
    if(B7 < 0x80000000) {
        p = (B7 * 2) / B4;
    } else {
        p = (B7 / B4) * 2;
    }
    X1 = (p / TWO_EIGHT) * (p / TWO_EIGHT);
    X1 = (X1 * 3038) / TWO_SIXTEEN;
    X2 = (-7357 * p) / TWO_SIXTEEN;
    p = p + (X1 + X2 + 3791) / TWO_FOUR;

    *pressure = (double)p / 1000.0;
    *temperature = (double)T / 10.0;

    free(buffer);
}
