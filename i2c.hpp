/*
 * i2c.hpp
 *
 *  Created on: Jul. 19, 2020
 *      Author: amr
 */

/** \file i2c.hpp
 * \brief The header file for the i2c class
 *
 */

#ifndef I2C_HPP_
#define I2C_HPP_

#include <stdint.h>
#include "enums.hpp"

#define ACK_DELAY 1450


/** \class i2c
 *  \brief A class for using the USCI module in I2C mode
 *
 */
class i2c {
  public:
    i2c(uint32_t i2cspeed, uint16_t perAddr = 0);
    uint8_t transact(uint8_t *data, uint16_t length, transactionType type);
    void changeAddress(uint8_t perAddr);
};

#endif /* I2C_HPP_ */
