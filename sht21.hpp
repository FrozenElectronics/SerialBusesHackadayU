/*
 * sht21.hpp
 *
 *  Created on: Jul. 22, 2020
 *      Author: amr
 */

#ifndef SHT21_HPP_
#define SHT21_HPP_

#include <i2c.hpp>

enum {
    TRIGGER_TEMP = 0xF3,
    TRIGGER_HUMID = 0xF5,
    WRITE_REG = 0xE6,
    READ_REG = 0xE7,
    SOFT_RESET = 0xFE
};

#define sht21Address 0x40

class sht21 {
  public:
    sht21(i2c *givenBus);
    i2c *busAccess;
    double getTemp(void);
    double getHumid(void);
    void heaterOn(void);
    void heaterOff(void);
};

#endif /* SHT21_HPP_ */
