/*
 * uart.cpp
 *
 *  Created on: Jul. 20, 2020
 *      Author: amr
 */

#include <msp430.h>
#include <stdint.h>
#include <math.h>
#include <uart.hpp>
/** \fn uart::uart
 *  \brief Constructor function for the uart class
 *  \param baud The requested baud rate
 *
 *  This function sets up the UART, and calculates the baud rate divisor based on the requested baud rate given.
 *  If it is unable to use the requested baud rate, it will calculate the next lowest, and repeat as necessary
 */
uart::uart(uint32_t baud) {
    extern uint32_t smClkSpeed;
    uint16_t modulation;
    UCA1CTL1 |= UCSWRST; // put USCI1 in reset mode
    UCA1CTL1 |= UCSSEL__SMCLK;
    // calculate baud rate
    double divisor = (double)smClkSpeed / (double)baud;
    uint16_t intPortion = (uint16_t)(divisor - 0.5);
    double fracPortion = divisor - (double)intPortion;
    if(intPortion >= 16) {
        divisor /= 16;
        intPortion /= 16;
        modulation = round((divisor - (double)intPortion) * 16);
        UCA1BRW = intPortion;
        UCA1MCTL = UCOS16 | (modulation << 4);
    } else {
        modulation = round(fracPortion * 8);
        UCA1BRW = intPortion;
        UCA1MCTL = (modulation << 4);
    }
    P4SEL |= BIT4 | BIT5; // set up UART pins, 4.4 and 4.5
    UCA1CTL1 &= ~UCSWRST; // activate
}

uart::~uart() {
    P4SEL &= ~(BIT4 | BIT5); // disable UART pins
}

void uart::CRLF(void) {
    const char *CR = "\r";
    const char *LF = "\n";
    UCA1TXBUF = *CR;
    while(!(UCA1IFG & UCTXIFG));
    UCA1TXBUF = *LF;
    while(!(UCA1IFG & UCTXIFG));
    return;
}

void uart::debugInfo(char *str, uint16_t length) {
    for(uint16_t i = 0; i < length; i++) {
        UCA1TXBUF = *(str + i);
        while(!(UCA1IFG & UCTXIFG)) ; // wait for buffer to empty
    }
    return;
}

