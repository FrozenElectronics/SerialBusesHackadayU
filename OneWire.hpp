/*
 * 1Wire.hpp
 *
 *  Created on: Aug. 12, 2020
 *      Author: amr
 */

#ifndef ONEWIRE_HPP_
#define ONEWIRE_HPP_

#include <stdint.h>
#include <stdlib.h>
#include "enums.hpp"

enum commandType {
    READ_ROM = 0x33,
    MATCH_ROM = 0x55,
    SKIP_ROM = 0xCC,
    SEARCH_ROM = 0xF0
};

#define RESET_LENGTH 9800
#define PRESENCE_LENGTH 1100
#define READ_LENGTH 30
#define READ_WAIT_LENGTH 180
#define READ_POST_LENGTH 1800
#define ONE_LOW_LENGTH 180
#define ONE_HIGH_LENGTH 2100
#define ZERO_LOW_LENGTH 1500
#define ZERO_HIGH_LENGTH 1100
#define BIT_PERIOD_LENGTH 80

class OneWire {
  public:
    OneWire();
    uint8_t resetBus();
    void transact(uint8_t *buffer, uint16_t length, transactionType type);
    uint8_t compareROM(uint8_t *storedROM, uint8_t *receivedROM);
};



#endif /* 1WIRE_HPP_ */
