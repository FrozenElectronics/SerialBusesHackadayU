/*
 * LCD.cpp
 *
 *  Created on: Sep. 27, 2020
 *      Author: amr
 */

#include <LCD.hpp>
#include <msp430.h>
#include <intrinsics.h>

LCD::LCD(i2c *givenBus, uint8_t lineWidth) {
    address = LCDAddr;
    width = lineWidth;
    lcdOut = 0x20; // 4-bit
    send(1);
    lcdOut = (functionSet | 0x08); // 4-bit, 2 lines, 5x7 dots
    send(1);
    lcdOut = (cursorShift | 0x00);
    send(1);
    lcdOut = (entryMode | 0x02); // cursor increment
    send(1);

    lcdOut = (displayOnOff | 0x04); // display on, cursor off, blink off
    send(1);
    lcdOut = (cursorHome);
    send(1);
    lcdOut = (homeInstruction);
    send(1);
}

void LCD::update(void) {
    uint_fast8_t charPosition = 0;	// this represents where we are on each line
    uint_fast8_t ddrPosition = 0;	// only needs to change once, to switch lines as
    // the cursor auto-increments for us
    lcdOut = (ddrAddress);
    send(1);

    for(charPosition = 0; charPosition < width; charPosition++) {
        lcdOut = lineOne[charPosition];
        if(lcdOut == 0x00) lcdOut = 0x20;
        send(0);
    }
    ddrPosition = 0x40; // move to second line
    lcdOut = (ddrAddress | ddrPosition); // move cursor to second line
    send(1);

    for(charPosition = 0; charPosition < width; charPosition++) {
        lcdOut = lineTwo[charPosition];
        if(lcdOut == 0x00) lcdOut = 0x20;
        send(0);
    }
}

void LCD::send(char command) {
    uint8_t sendBuf[6];
    busAccess->changeAddress(address);
    if(command) {
        sendBuf[0] = ((lcdOut & 0xF0) | 0x08);
        sendBuf[1] = ((lcdOut & 0xF0) | 0x0C);
        sendBuf[2] = ((lcdOut & 0xF0) | 0x08);
        sendBuf[3] = (((lcdOut & 0x0F) << 4) | 0x08);
        sendBuf[4] = (((lcdOut & 0x0F) << 4) | 0x0C);
        sendBuf[5] = (((lcdOut & 0x0F) << 4) | 0x08);
        busAccess->transact(&sendBuf[0], 6, WRITE); // fire off the commands
    } else {
        sendBuf[0] = ((lcdOut & 0xF0) | 0x09);
        sendBuf[1] = ((lcdOut & 0xF0) | 0x0D);
        sendBuf[2] = ((lcdOut & 0xF0) | 0x09);
        sendBuf[3] = (((lcdOut & 0x0F) << 4) | 0x09);
        sendBuf[4] = (((lcdOut & 0x0F) << 4) | 0x0D);
        sendBuf[5] = (((lcdOut & 0x0F) << 4) | 0x09);
        busAccess->transact(&sendBuf[0], 6, WRITE); // fire off the commands
    }
    __delay_cycles(1000); // small delay
}
