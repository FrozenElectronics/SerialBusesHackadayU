/*
 * enums.hpp
 *
 *  Created on: Aug. 12, 2020
 *      Author: amr
 */

#ifndef ENUMS_HPP_
#define ENUMS_HPP_

enum transactionType {
    WRITE = 0, /**< \enum WRITE denotes a write operation for transact() */
    READ = 1, /**< \enum READ denotes a read operation for transact() */
    WRITEREAD = 2, /**< \enum WRITEREAD denotes a single byte write followed by a multi-byte read */
    TEST = 3 /**< \enum TEST starts the test mode in transact() */
};

enum errorType {
    NACK_DATA = 0, /**< \enum NACK_DATA a return code used if the peripheral sent a NACK on the transmitted data */
    NACK_ADDR = 1, /**< \enum NACK_ADDR a return code used if the peripheral sent a NACK on the transmitted address */
    SUCCESS = 2 /**< \enum SUCCESS a return code indicating the operation was successful */
};


#endif /* ENUMS_HPP_ */
