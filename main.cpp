#include <msp430.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include "uart.hpp"
#include "sht21.hpp"
#include "OneWire.hpp"
#include "bmp180.hpp"
#include "LCD.hpp"

/**
 * main.c
 */

/* Global variables
 * Yes, very bad
 * slap my wrist
 */
/*** DO NOT FORGET TO CHANGE THESE IF YOU CHANGE THE CLOCK SETUP!!!!!!! ***/
extern uint32_t smClkSpeed = 5e6, mClkSpeed = 20e6, aClkSpeed = 32768; // @suppress("Multiple variable declaration")

// SetVCoreUp() copied from TI example code and therefore require this license notification:
/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************/
void SetVcoreUp(unsigned int level) {
    // Open PMM registers for write
    PMMCTL0_H = PMMPW_H;
    // Set SVS/SVM high side new level
    SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;
    // Set SVM low side to new level
    SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;
    // Wait till SVM is settled
    while ((PMMIFG & SVSMLDLYIFG) == 0) ;
    // Clear already set flags
    PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);
    // Set VCore to new level
    PMMCTL0_L = PMMCOREV0 * level;
    // Wait till new level reached
    if ((PMMIFG & SVMLIFG)) while ((PMMIFG & SVMLVLRIFG) == 0) ;
    // Set SVS/SVM low side to new level
    SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;
    // Lock PMM registers for write access
    PMMCTL0_H = 0x00;
}

// Everything besides that last function is under Mozilla Public License v2

void clockSetup(void) {
    /*
     * Set clocks as follows:
     * Enable XT2 (4MHz)
     * Set XT2 as FLL source
     * Set DCO to 20MHz?
     */
    P5SEL |= BIT2 + BIT3;                       // Port select XT2
    P5SEL |= BIT4 + BIT5;                       // Port select XT1

    UCSCTL6 &= ~XT2OFF;                       // Set XT2 On
    UCSCTL6 &= ~XT1OFF;                       // Set XT1 On
    UCSCTL6 |= XCAP_3;                        // Internal load cap
    do {
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
        // Clear XT2,XT1,DCO fault flags
        SFRIFG1 &= ~OFIFG;                      // Clear fault flags
    } while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag
    SetVcoreUp(0x01);
    SetVcoreUp(0x02);
    SetVcoreUp(0x03); // the core has to be set to its highest power level to allow fast clocks
    UCSCTL0 = 0x00;
    UCSCTL3 = SELREF__XT2CLK | FLLREFDIV__4; // 1MHz into FLL
    __delay_cycles(25000);
    __bis_SR_register(SCG0); // disable FLL loop
    UCSCTL1 = DCORSEL_5; // 2.5 - 23.7MHz range
    UCSCTL2 = 19; // 19+1, 20MHz output
    __bic_SR_register(SCG0);
    __delay_cycles(25000); // wait for clock to settle?
    UCSCTL4 = SELA__REFOCLK | SELS__DCOCLK | SELM__DCOCLK;
    UCSCTL5 = DIVS__4 | DIVM__1 | DIVA__1; // set SMCLK to 5MHz, MCLK to 20MHz, ACLK to 32768Hz
    aClkSpeed = 32768;
    return;
}

void fastClock(void) {
    SetVcoreUp(0x01);
    SetVcoreUp(0x02);
    SetVcoreUp(0x03); // the core has to be set to its highest power level to allow fast clocks
    UCSCTL0 = 0x00;
    UCSCTL3 = SELREF__XT2CLK | FLLREFDIV__4; // 1MHz into FLL
    __delay_cycles(25000);
    __bis_SR_register(SCG0); // disable FLL loop
    UCSCTL1 = DCORSEL_5; // 2.5 - 23.7MHz range
    UCSCTL2 = 19; // 19+1, 20MHz output
    __bic_SR_register(SCG0);
    __delay_cycles(25000); // wait for clock to settle?
    UCSCTL4 = SELA__REFOCLK | SELS__DCOCLK | SELM__DCOCLK;
    UCSCTL5 = DIVS__4 | DIVM__1 | DIVA__1; // set SMCLK to 5MHz, MCLK to 20MHz, ACLK to 32768Hz
    aClkSpeed = 32768;
    return;
}

void slowClock(void) {
    UCSCTL0 = 0x00;
    UCSCTL3 = SELREF__XT2CLK | FLLREFDIV__8; // 0.5MHz into FLL
    __delay_cycles(25000);
    __bis_SR_register(SCG0); // disable FLL loop
    UCSCTL1 = DCORSEL_1; // 0.15-1.47MHz range
    UCSCTL2 = 1; // 1+1, 1MHz output?
    __bic_SR_register(SCG0);
    __delay_cycles(25000); // wait for clock to settle?
    UCSCTL4 = SELA__VLOCLK | SELS__DCOCLK | SELM__DCOCLK;
    UCSCTL5 = DIVS__4 | DIVM__1 | DIVA__2; // set SMCLK/MCLK to 1MHz, ACLK to 5000Hz
    aClkSpeed = 5000;
    SetVcoreUp(0x02);
    SetVcoreUp(0x01);
    SetVcoreUp(0x00); // back to lowest power level

    return;
}

void main(void) {
//    const uint8_t storedSerialNumber[8] = { 0x01, 0xC7, 0x54, 0x7C, 0x1B, 0x00, 0x00, 0x9C };
//    uint8_t *serialNumber = (uint8_t *)malloc(sizeof(uint8_t) * 8); // allocate space for the 8-byte serial number
//    uint8_t skipRom = 0x33;

    double humidity = 0;
    double temperature = 0;
    double pressure = 0;

    WDTCTL = WDTPW | WDTHOLD; // stop the watchdog
    char *stringBuffer = (char *)calloc(50, sizeof(char)); // create a 50 byte string buffer
    uint16_t stringLength = 0;

    clockSetup();

    P1DIR |= 0x01;
    P1DS |= 0x01; // set high drive strength

    //OneWire owb;
    i2c i2cBus(400e3);
    sht21 sht(&i2cBus);
    bmp180 bmp(&i2cBus);
    LCD display(&i2cBus, 16);
    uart console(115200);
    TA0CCTL0 = CCIE;
    //TA0CCR0 = 0x927C; // 37500 -- at 625Hz this is exactly 60 seconds (measured about 65.5 @ 22 degrees or so)
    TA0CCR0 = 0x0927;
    TA0CTL = TASSEL__ACLK + ID__8 + TACLR;

//    // Get and compare the serial number to the stored one
//    owb.resetBus();
//    owb.transact(&skipRom, 1, WRITE);
//    owb.transact(serialNumber, 8, READ);
//    if(!owb.compareROM((uint8_t *)storedSerialNumber, serialNumber)) {
//        stringLength = sprintf(stringBuffer, "Critical error: ROM Number Mismatch!\r\nAll your base are belong to us.");
//        console.debugInfo(stringBuffer, stringLength);
//        return; // crash the processor?
//    }
//    __delay_cycles(400e3);

    while (1) {
        humidity = 0;
        temperature = 0;
        humidity = sht.getHumid();
        temperature = sht.getTemp();
        // Each line must be *exactly* 20 characters
        stringLength = sprintf(display.lineOne, "H%02.2f%% T%02.2fC", humidity, temperature); // @suppress("Invalid arguments")
        console.debugInfo(display.lineOne, stringLength);
        console.CRLF();
        bmp.getPressure(&pressure, &temperature);
        stringLength = sprintf(display.lineTwo, "P: %02.3fkPa", pressure);
        console.debugInfo(display.lineTwo, stringLength);
        console.CRLF();
        display.update();

        TA0CTL |= MC__UP; // start timer
        slowClock();
        __bis_SR_register(LPM1_bits + GIE);
        __no_operation();
        fastClock();
    }
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(TIMER0_A0_VECTOR))) TIMER0_A0_ISR (void)
#else
#error Compiler not supported!
#endif
{
    __disable_interrupt();
    TA0CTL &= ~MC__UP; // turn timer off
    P1OUT ^= 0x01;
    __bic_SR_register_on_exit(LPM1_bits);

}
