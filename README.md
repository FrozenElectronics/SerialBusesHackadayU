# Embedded Serial Buses HackadayU Course
## Example Code by Alexander Rowsell

### Description

This repository contains an I2C and 1-Wire library for the [Embedded Serial Buses HackadayU course](https://hackaday.io/project/173667-embedded-serial-buses). Different branches and tags
represent different versions of the code, as the lessons develop.

For Week 2, check the [sht_21](https://gitlab.com/FrozenElectronics/SerialBusesHackadayU/-/tree/sht21) or the [sht_31](https://gitlab.com/FrozenElectronics/SerialBusesHackadayU/-/tree/sht23) branch depending on which chip you have. 
This contains basic code for just the SHT21, as seen in Week 2.

For week 3, check out the [week3](https://gitlab.com/FrozenElectronics/SerialBusesHackadayU/-/tree/week3) branch. It contains the OneWire library with a basic example with the DS2401. 

The master branch contains code with both the 1-Wire and I2C library, plus examples for the SHT21 and BMP180.

If you have any questions, feel free to contact me here through the Issues system, on [hackaday.io](https://hackaday.io/MrAureliusR), or email me
at the email address included in the lesson slides.

### Code Overview

This example code is all written in C++. The I2C and 1-Wire libraries are implemented as classes. Instances of these classes
are then passed to the classes that represent the sensors we communicate with, making the user-side code quite simple.