/*
 * uart.h
 *
 *  Created on: Jul. 20, 2020
 *      Author: amr
 */

#ifndef UART_HPP_
#define UART_HPP_

#include <cstdint>

class uart {
  public:
    uart(uint32_t baud);
    virtual ~uart();
    void debugInfo(char *str, uint16_t length);
    void CRLF(void);
};

#endif /* UART_HPP_ */
