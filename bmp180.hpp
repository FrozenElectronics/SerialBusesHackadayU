/*
 * bmp180.hpp
 *
 *  Created on: Aug. 15, 2020
 *      Author: amr
 */

#ifndef BMP180_HPP_
#define BMP180_HPP_
#include <i2c.hpp>

#define bmp180Addr 0x77

enum bmpCommands {
    SAMPLE_TEMPERATURE = 0x2E,
    SAMPLE_PRESSURE = 0xF4
};

class bmp180 {
  public:
    bmp180(i2c *givenBus);
    void getPressure(double *pressure, double *temperature);
    i2c *busAccess;

  private:
    int16_t AC1, AC2, AC3;
    uint16_t AC4, AC5, AC6;
    int16_t B1, B2;
    int16_t MB, MC, MD;
};

#endif /* BMP180_HPP_ */
