/*
 * LCD.hpp
 *
 *  Created on: Sep. 27, 2020
 *      Author: amr
 */

#ifndef LCD_HPP_
#define LCD_HPP_

#include <i2c.hpp>

#define LCDAddr 0x27

#define homeInstruction 0x01
#define cursorHome 		0x02
#define entryMode		0x04
#define displayOnOff 	0x08
#define cursorShift		0x10
#define functionSet 	0x20
#define ddrAddress		0x80

class LCD {
  public:
    LCD(i2c *givenBus, uint8_t lineWidth);
    void update(void);
    i2c *busAccess;
    uint8_t address;
    uint8_t lcdOut;
    char lineOne[20];
    char lineTwo[20];
    uint8_t width;
  private:
    void send(char command);
};

#endif /* LCD_HPP_ */
