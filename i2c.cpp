/*
 * i2c.cpp
 *
 *  Created on: Jul. 19, 2020
 *      Author: amr
 */
#include <msp430.h>
#include <intrinsics.h>
#include <i2c.hpp>

/** \fn i2c::i2c
 * \brief Enable I2C module.
 * \param perAddr The 7-bit peripheral address
 * \param i2cspeed The requested I2C speed, typically 100e3 or 400e3
 * This is the constructor function of the I2C class.
 * Giving it the required speed, it will auto-calculate the baud rate divisor using the smClkSpeed variable from main.c.
 * Also, when we create an instance, the peripheral's address is passed. It is put in the I2C address register and
 * we don't need to worry about it again.
 * \sa i2c::changeAddress()
 */
i2c::i2c(uint32_t i2cspeed, uint16_t perAddr) {
    extern uint32_t smClkSpeed;
    double baud = ((double)smClkSpeed / (double)i2cspeed);
    uint8_t ibaud = (uint8_t)(baud + 0.5); // yes this works
    P3DIR &= ~(BIT0 | BIT1);
    P3SEL |= BIT0 | BIT1; // set 3.0 and 3.1 to SDA and SCL
    UCB0CTL1 |= UCSWRST; // UCSWRST, USCI in reset state
    UCB0CTL0 = UCMST + UCMODE_3 + UCSYNC; // transmitter mode
    UCB0CTL1 = UCSSEL__SMCLK + UCSWRST; // use SMCLK
    UCB0BR0 = (uint8_t)(ibaud & 0x00FF);
    UCB0BR1 = (uint8_t)((ibaud & 0xFF00) >> 8);

    if (perAddr != 0) {
        UCB0I2CSA = perAddr; // set slave address
    }

    UCB0CTL1 &= ~UCSWRST; // activate USCI
}

/** \fn i2c::transact
 *  \brief Send or receive data with the I2C module
 *  \param *data A pointer to the data buffer
 *  \param length The number of bytes to be read/written
 *  \param type One of WRITE, READ, WRITEREAD or TEST, as defined in i2c.hpp
 *
 *  This function can clobber whatever is in *data, so make sure it is empty or
 *  contains data that has already been read.
 *
 */
uint8_t i2c::transact(uint8_t *data, uint16_t length, transactionType type) {
    uint16_t i = 0;
    uint8_t dummyByte;
    const int max = 255;
    const int prime = 317;
    switch (type) {
    case WRITE:
        UCB0CTL1 |= UCTR + UCTXSTT; // send a START and address
        while (!(UCB0IFG & UCTXIFG)) ;	// wait until data can be written
        UCB0TXBUF = *(data + i); // we have to send the first byte to be able to check if an address nack happened
        while(UCB0CTL1 & UCTXSTT) ; // wait for start condition done
        if (!(UCB0CTL1 & UCTXSTT) && (UCB0IFG & UCNACKIFG)) {
            UCB0CTL1 |= UCTXSTP;
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            return NACK_ADDR;
        }
        while(!(UCB0IFG & UCTXIFG)) ; // wait for buffer to be clear
        for (i = 1; i < length; i++) {
            UCB0TXBUF = *(data + i); // send data
            while(!(UCB0IFG & UCTXIFG)) ; // wait for buffer to be clear
            if (UCB0IFG & UCNACKIFG) { // the peripheral nack'd
                UCB0CTL1 |= UCTXSTP; // send a stop
                while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
                return NACK_DATA;
            }
        }
        UCB0CTL1 |= UCTXSTP; // send stop
        break;
    case READ:
        UCB0CTL1 &= ~UCTR; // set listener mode
        UCB0CTL1 |= UCTXSTT; // send a START
        while(UCB0CTL1 & UCTXSTT) ; // wait for start condition done
        if (!(UCB0CTL1 & UCTXSTT) && (UCB0IFG & UCNACKIFG)) { // peripheral n'acked on address
            UCB0CTL1 |= UCTXSTP;
            while(UCB0CTL1 & UCTXSTP) ;
            return NACK_ADDR;
        }
        while (!(UCB0IFG & UCRXIFG)) ;
        if (length == 1) {
            UCB0CTL1 |= UCTXSTP; // we must set the STOP bit while the first byte is being read,
            // if it's a single byte only read
            UCB0IFG &= ~UCTXIFG;
            *data = UCB0RXBUF;
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            return SUCCESS;
        } else {
            for(i = 0; i < length; i++) {
                if(i == length - 1) { // if we are on the last byte, it's a special case
                    UCB0CTL1 |= UCTXSTP; // send stop
                    *(data + i) = UCB0RXBUF; // read byte
                    UCB0IFG &= ~UCTXIFG;
                    while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
                    break;
                }
                while (!(UCB0IFG & UCRXIFG)) ; // wait for next byte to come
                *(data + i) = UCB0RXBUF; // read byte
            }
            dummyByte = UCB0RXBUF; // read a dummy byte to prevent bug
        }
        break;
    case WRITEREAD:
        // Start of transmit //
        UCB0CTL1 |= UCTR + UCTXSTT; // send a START and address
        while (!(UCB0IFG & UCTXIFG)) ;	// wait until data can be written
        UCB0TXBUF = *(data + i); // we have to send the first byte to be able to check if an address nack happened
        while(UCB0CTL1 & UCTXSTT) ; // wait for start condition done
        if (!(UCB0CTL1 & UCTXSTT) && (UCB0IFG & UCNACKIFG)) {
            UCB0CTL1 |= UCTXSTP;
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            return NACK_ADDR;
        }
        for (i = 1; i < length; i++) {
            UCB0TXBUF = *(data + i); // send data
            while(!(UCB0IFG & UCTXIFG)) ; // wait for buffer to be clear
            if (UCB0IFG & UCNACKIFG) { // the peripheral nack'd
                UCB0CTL1 |= UCTXSTP; // send a stop
                UCB0IFG &= ~UCTXIFG;
                while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
                return NACK_DATA;
            }
        }
        // Start of receive //
        UCB0CTL1 &= ~UCTR; // listener mode
        UCB0CTL1 |= UCTXSTT; // repeated start
        while (!(UCB0IFG & UCTXIFG)) ; // wait for start to be generated
        if (!(UCB0CTL1 & UCTXSTT) && (UCB0IFG & UCNACKIFG)) { // peripheral n'acked on address
            UCB0CTL1 |= UCTXSTP;
            UCB0IFG &= ~UCTXIFG;
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            return NACK_ADDR;
        }
        if (length == 1) {
            UCB0CTL1 |= UCTXSTP; // we must set the STOP bit while the first byte is being read,
            // if it's a single byte only read
            *data = UCB0RXBUF;
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            return SUCCESS;
        } else {
            for (uint16_t i = 0; i < length; i++) {
                while (!(UCB0IFG & UCRXIFG))
                    ; // wait for first byte to come
                *(data + i) = UCB0RXBUF; // read byte
            }
            UCB0CTL1 |= UCTXSTP; // send stop
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
            dummyByte = UCB0RXBUF; // empty the buffer
        }
        break;
    case TEST:
        do {
            UCB0CTL1 |= UCTR + UCTXSTT; // send a START and address
            while (!(UCB0IFG & UCTXIFG)) ;	// wait until data can be written
            for (int i = 0; i < max; ++i) {
                uint8_t c = (int) (i * prime) % max;
                UCB0TXBUF = c;
                while (!(UCB0IFG & UCTXIFG)) ;	// wait until data can be written
            }
            UCB0CTL1 |= UCTXSTP; // send stop
            while(UCB0CTL1 & UCTXSTP) ; // wait for STOP to complete
        } while (length--);
        break;
    default:
        __never_executed();
        break;
    }
    return SUCCESS;
}

void i2c::changeAddress(uint8_t perAddr) {
    //UCB0CTL1 |= UCSWRST;
    UCB0I2CSA = perAddr;
    //UCB0CTL1 &= ~UCSWRST;
    return;
}
